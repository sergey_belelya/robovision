# Robovision - клиент для приема видеопотока от распберри по UDP протоколу.


## Как собрать:

1. Открываем проект в Android Studio.
2. Редактируем `local.properties` - устанавливаем Android SDK и NDK (r10e). ([Ссылка на NDK](http://stackoverflow.com/a/28088215)). Возможно блокировка со стороны провайдера - обходим через прокси (friGate например).
3. Скачиваем GStreamer Android library : [Gstreamer](http://gstreamer.freedesktop.org/data/pkg/android/). Распаковываем куда-нибудь.
4. Редактируем `src/main/jni/Android.mk` и указываем `GSTREAMER_ROOT_ANDROID` путь к распакованному Gstreamer library.
5. Запускаем.

### Замечания ###

Собирать только Gradle v.1.0.0 (`build.gradle`). Желательно не трогать файлы gradle без необходимости.

Прием видеопотока с распберри настраивается в `src/main/jni/native-code.c` в строчке: 
```
#!C++
data->pipeline = gst_parse_launch("udpsrc port=5001 ! queue2 max-size-buffers=1 ! decodebin ! autovideosink sync=false", &error);
```